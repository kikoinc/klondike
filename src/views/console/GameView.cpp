#include "views/console/GameView.hpp"

void ConsoleView::GameView::interact(MovementControllerInterface* moveController) {
    const Error* error = askMovementView.interact(moveController);

    if (!error) {
        moveController->move();
    } else {
        io.writeLine("ERROR: " + error->getMessage());
        io.writeLine("Please, try again.");
    }
    ConsoleView::DashboardView(moveController).write();
}

void ConsoleView::GameView::printBadMovement() {
    io.writeLine("Movimiento no permitido");
}

void ConsoleView::GameView::printEndGame() {
    io.writeLine("Victoria!!!");
}
