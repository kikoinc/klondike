#include "views/console/DashboardView.hpp"
#include "views/console/PileView.hpp"

ConsoleView::DashboardView::DashboardView(PresenterControllerInterface* presenterController) {
    assert (presenterController != nullptr);

    this->presenterController = presenterController;
}

void ConsoleView::DashboardView::write() {
    IO io = IO();

    std::string unturnedStr("UNTURNED");
    std::string wasteStr("WASTE");
    std::string foundationStr("FOUNDATION");
    std::string tableauStr("TABLEAU");

    PileView pileViewUnturned = PileView(presenterController->getDashboard()->getUnturned());
    pileViewUnturned.write(unturnedStr + ": ");

    PileView pileViewWaste = PileView(presenterController->getDashboard()->getWaste());
    pileViewWaste.write(wasteStr + ": ");
    io.writeLine("");

    for (int i = 0; i < Dashboard::NUMBER_FOUNDATIONS; i++) {
        PileView pileViewFoundation = PileView(presenterController->getDashboard()->getFoundation(i));
        pileViewFoundation.write(foundationStr + " " + std::to_string(i+1) + ": ");
    }
    io.writeLine("");

    for (int i = 0; i < Dashboard::NUMBER_TABLEAUS; i++) {
        PileView pileViewTableau = PileView(presenterController->getDashboard()->getTableau(i));
        pileViewTableau.write(tableauStr + " " + std::to_string(i+1) + ": ");
    }
    io.writeLine("");
}
