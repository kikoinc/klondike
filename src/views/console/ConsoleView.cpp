#include "views/console/ConsoleView.hpp"

void ConsoleView::ConsoleView::interact(ControllerInterface* controller) {
    assert (controller != nullptr);

    controller->accept(this);
}

void ConsoleView::ConsoleView::visit(StartControllerInterface* startController) {
    startView.interact(startController);
}

void ConsoleView::ConsoleView::visit(MovementControllerInterface* movementController) {
    gameView.interact(movementController);
}

void ConsoleView::ConsoleView::visit(WinControllerInterface* winController) {
    winView.interact(winController);
}

