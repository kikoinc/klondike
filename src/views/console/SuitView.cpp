#include "views/console/SuitView.hpp"

const std::string GRAPH_CLUBS = "C_B";
const std::string GRAPH_DIAMONDS = "D_R";
const std::string GRAPH_HEARTS = "H_R";
const std::string GRAPH_SPADES = "S_B";

ConsoleView::SuitView::SuitView(Suit suit):
    suit(suit) {
}

void ConsoleView::SuitView::write() {
    if (suit == Suit::CLUBS) {
        io.write(GRAPH_CLUBS);
    } else if (suit == Suit::DIAMONDS) {
        io.write(GRAPH_DIAMONDS);
    } else if (suit == Suit::HEARTS) {
        io.write(GRAPH_HEARTS);
    } else {
        io.write(GRAPH_SPADES);
    }
}