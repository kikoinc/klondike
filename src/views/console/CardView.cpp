#include "views/console/CardView.hpp"
#include "views/console/SuitView.hpp"

ConsoleView::CardView::CardView(Card card):
    card(card) {
}

void ConsoleView::CardView::write() {
    IO io = IO();

    if (card.isTurned()) {
        io.write("[");
        io.write(card.getValue());
        SuitView suitView = SuitView(card.getSuit());
        suitView.write();
        io.write("]");
    } else {
        io.write("[XX]");
    }
}