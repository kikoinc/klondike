#include "views/console/PileView.hpp"
#include "views/console/CardView.hpp"

ConsoleView::PileView::PileView(Pile* pile) {
    assert(pile != nullptr);

    this->pile = pile;
}

void ConsoleView::PileView::write(std::string title) {
    io.write(title);
    if (pile->isEmpty()) {
        io.write("-");
    } else {
        pile->accept(this);
    }
    io.write("\n");
}

void ConsoleView::PileView::visit(Unturned* unturned) {
    printCard(unturned->getTopCard());
}

void ConsoleView::PileView::visit(Waste* waste) {
    printCard(waste->getTopCard());
}

void ConsoleView::PileView::visit(Foundation* foundation) {
    for (Card card: foundation->getCards()) {
        printCard(card);
    }
}

void ConsoleView::PileView::visit(Tableau* tableau) {
    for (Card card: tableau->getCards()) {
        printCard(card);
    }
}

void ConsoleView::PileView::printCard(Card card) {
    ConsoleView::CardView cardView = ConsoleView::CardView(card);
    cardView.write();
}