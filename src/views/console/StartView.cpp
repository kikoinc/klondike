#include "views/console/StartView.hpp"

void ConsoleView::StartView::interact(StartControllerInterface* startController) {
    startController->start();
    ConsoleView::DashboardView(startController).write();
}