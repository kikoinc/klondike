#include "views/console/AskMovementView.hpp"

const int NUM_MOVEMENTS = 2;
const int NUM_PILES = 3;

const Error* ConsoleView::AskMovementView::interact(MovementControllerInterface* moveController) {
    assert(moveController != nullptr);

    this->moveController = moveController;

    printAvailableMovements();
    LimitedIntDialog intDialog = LimitedIntDialog("Select a movement:", 1, NUM_MOVEMENTS);

    Pile* origin = nullptr;
    Pile* destination = nullptr;

    if (intDialog.read() == 1) {
        origin = moveController->getDashboard()->getUnturned();
        destination = moveController->getDashboard()->getWaste();
    } else {
        origin = askForPile("Select origin pile:");
        destination = askForPile("Select destination pile:");
    }

    const Error* error = moveController->validateMovement(origin, destination);

    if (!error) {
        moveController->setOrigin(origin);
        moveController->setDestination(destination);
    }

    return error;
}


Pile* ConsoleView::AskMovementView::askForPile(std::string title) {
    Pile* returnPile;
    bool ok = false;

    do {
        printAvailablePiles(title);
        LimitedIntDialog intDialog = LimitedIntDialog("Select a pile:", 1, NUM_PILES);

        switch(intDialog.read()) {
            case 1: {
                returnPile = moveController->getDashboard()->getWaste();
                ok = true;
                break;
            }
            case 2: {
                printPilesPerType("FOUNDATION", Dashboard::NUMBER_FOUNDATIONS);
                LimitedIntDialog foundationDialog = LimitedIntDialog("Select a foundation:", 1,
                                                                     Dashboard::NUMBER_FOUNDATIONS);
                returnPile = moveController->getDashboard()->getFoundation(foundationDialog.read() - 1);
                ok = true;
                break;
            }
            case 3: {
                printPilesPerType("TABLEAU", Dashboard::NUMBER_TABLEAUS);
                LimitedIntDialog tableauDialog = LimitedIntDialog("Select a tableau:", 1,
                                                                  Dashboard::NUMBER_TABLEAUS);
                returnPile = moveController->getDashboard()->getTableau(tableauDialog.read() - 1);
                ok = true;
                break;
            }
            default:
                break;
        }
    } while (!ok);

    return returnPile;
}

void ConsoleView::AskMovementView::printAvailableMovements() {
    io.writeLine("Available Movements: ");
    io.writeLine("\t1. Take a card from the deck.");
    io.writeLine("\t2. Move card between piles.");
}

void ConsoleView::AskMovementView::printAvailablePiles(std::string title) {
    io.writeLine(title);
    io.writeLine("\t1. WASTE");
    io.writeLine("\t2. FOUNDATION");
    io.writeLine("\t3. TABLEAU");
}

void ConsoleView::AskMovementView::printPilesPerType(std::string pileName, int number) {
    io.writeLine("Available " + pileName + "S:");
    for (int i = 1; i <= number; i++) {
        io.write("\t");
        io.write(i);
        io.write(". " + pileName + " ");
        io.write(i);
        io.writeLine("");
    }
}
