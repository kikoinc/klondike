#include "utils/LimitedIntDialog.hpp"

LimitedIntDialog::LimitedIntDialog(std::string title, int min, int max):
    title(title) {
    interval = new ClosedInterval(min, max);
}

LimitedIntDialog::~LimitedIntDialog() {
    delete interval;
}

int LimitedIntDialog::read() {
    int value;
    bool ok = false;

    do {
        value = io.readInt(title);

        if (interval->includes(value)) {
            ok = true;
        } else {
            std::stringstream sstream;
            sstream << "El valor debe estar entre " << interval->getMin() << " y " << interval->getMax() << ".";
            io.writeLine(sstream.str());
        }
    } while(!ok);

    return value;
}
