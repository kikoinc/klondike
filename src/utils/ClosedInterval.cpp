#include "utils/ClosedInterval.hpp"

ClosedInterval::ClosedInterval(int min, int max):
    min(min), max(max) {
}

int ClosedInterval::getMin() {
    return min;
}

int ClosedInterval::getMax() {
    return max;
}

bool ClosedInterval:: includes(int value) {
    return min <= value && value <= max;
}
