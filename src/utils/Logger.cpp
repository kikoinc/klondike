#include "utils/Logger.hpp"

void Logger::debug(char const* message) {
    std::cout << "DEBUG: " << message << '\n';
}

void Logger::debug(std::string message) {
    std::cout << "DEBUG: " << message << '\n';
}

void Logger::debug(int n) {
    std::cout << "DEBUG: " << std::to_string(n) << '\n';
}