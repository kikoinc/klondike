#include "utils/IO.hpp"

std::string IO::readString(std::string title) {
    std::string str;

    std::cout << title;
    std::cin >> str;
    return str;
}

void IO::write(std::string string) {
    std::cout << string;
}

void IO::write(int n) {
    std::cout << std::to_string(n);
}

void IO::writeLine(std::string string) {
    write(string);
    std::cout << '\n';
}

int IO::readInt(std::string title) {
    bool ok = false;
    int number;

    do {
        try {
            number = std::stoi(readString(title));
            ok = true;
        } catch (std::exception& ex) {
            writeLine("Número incorrecto. Inténtelo de nuevo.");
        }
    } while (!ok);

    return number;
}