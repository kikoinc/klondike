#include "controllers/Controller.hpp"

Game* Controller::getGame() {
    return game;
}

void Controller::setGame(Game *game) {
    this->game = game;
}

Dashboard* Controller::getDashboard() {
    return this->game->getDashboard();
}

