#include "controllers/MovementController.hpp"

MovementController::MovementController(Game* game) {
    assert (game != nullptr);
    this->setGame(game);
}

void MovementController::move() {
    Card card = this->origin->getTopCard();

    if (!card.isTurned()) {
        card.turn();
    }
    this->destination->insertCard(card);
    this->origin->accept(this);

    if (this->getGame()->isWinner()) {
        this->getGame()->setState(&State::WIN);
    }
}

void MovementController::accept(ControllerVisitor* controllerVisitor) {
    assert(controllerVisitor != nullptr);

    controllerVisitor->visit(this);
}

Dashboard* MovementController::getDashboard() {
    return Controller::getDashboard();
}

void MovementController::setOrigin(Pile* origin) {
    assert(origin != nullptr);

    this->origin = origin;
}

void MovementController::setDestination(Pile* destination) {
    assert(destination != nullptr);

    this->destination = destination;
}

const Error* MovementController::validateMovement(Pile* origin, Pile* destination) {
    assert(origin != nullptr);
    assert(destination != nullptr);

    if (origin->isEmpty()) {
        return &Error::PILE_IS_EMPTY;
    }

    if (!destination->canInsert(origin->getTopCard())) {
        return &Error::MOVEMENT_NOT_ALLOWED;
    }

    return nullptr;
}

void MovementController::visit(Unturned* unturned) {
    assert(unturned != nullptr);

    unturned->removeTopCard();
}

void MovementController::visit(Waste* waste) {
    assert(waste != nullptr);

    waste->removeTopCard();
    if (!waste->isEmpty() && !waste->getTopCard().isTurned()) {
        Card card = waste->getTopCard();
        card.turn();
        waste->removeTopCard();
        waste->insertCard(card);
    }
}

void MovementController::visit(Foundation* foundation) {
    assert(foundation != nullptr);

    foundation->removeTopCard();
}

void MovementController::visit(Tableau* tableau) {
    assert(tableau != nullptr);

    tableau->removeTopCard();
    if (!tableau->isEmpty() && !tableau->getTopCard().isTurned()) {
        Card card = tableau->getTopCard();
        card.turn();
        tableau->removeTopCard();
        tableau->insertCard(card);
    }
}