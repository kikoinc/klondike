#include "controllers/Logic.hpp"

Logic::Logic() {
    startController = new StartController(&game);
    movementController = new MovementController(&game);
    winController = new WinController(&game);
}

Logic::~Logic() {
    delete startController;
    delete movementController;
    delete winController;
}

Controller* Logic::getController() {
    const State state = game.getState();

    if (state == State::START) {
        return startController;
    } else if (state == State::IN_GAME) {
        return movementController;
    } else if (state == State::WIN) {
        return winController;
    } else {
        return nullptr;
    }
}
