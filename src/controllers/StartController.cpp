#include "controllers/StartController.hpp"

StartController::StartController(Game* game) {
    assert (game != nullptr);
    this->setGame(game);
}

void StartController::start() {
    this->getGame()->setState(&State::IN_GAME);
}

void StartController::accept(ControllerVisitor* controllerVisitor) {
    controllerVisitor->visit(this);
}

Dashboard* StartController::getDashboard() {
    return Controller::getDashboard();
}
