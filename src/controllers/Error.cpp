#include "controllers/Error.hpp"

const Error Error::MOVEMENT_NOT_ALLOWED = Error("Movement not allowed.");
const Error Error::PILE_IS_EMPTY = Error("Pile is empty.");

Error::Error(std::string message):
        message(message) {
}

std::string Error::getMessage() const {
    return this->message;
}