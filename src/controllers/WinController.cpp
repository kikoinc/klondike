#include "controllers/WinController.hpp"

WinController::WinController(Game* game) {
    assert (game != nullptr);
    this->setGame(game);
}

void WinController::accept(ControllerVisitor* controllerVisitor) {
    controllerVisitor->visit(this);
}

void WinController::win() {
    this->getGame()->setState(&State::END);
}

Dashboard* WinController::getDashboard() {
    return Controller::getDashboard();
}