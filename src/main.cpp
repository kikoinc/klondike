#include "Klondike.hpp"
#include "views/console/ConsoleView.hpp"

int main() {
    ConsoleView::ConsoleView consoleView = ConsoleView::ConsoleView();
    Klondike klondike(&consoleView);
    klondike.play();
}