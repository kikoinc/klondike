#include "models/Game.hpp"
#include "../../include/models/Game.hpp"

Game::Game() {
    state = &(State::START);
    dashboard = new Dashboard();
}

Game::~Game() {
    delete state;
    delete dashboard;
}

const State Game::getState() {
    return *state;
}

void Game::setState(const State* state) {
    this->state = state;
}

Dashboard* Game::getDashboard() {
    return dashboard;
}

bool Game::isWinner() {
    return dashboard->is_winner();
}


