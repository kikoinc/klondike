#include "models/Tableau.hpp"
#include "PileVisitor.hpp"

Tableau::Tableau(Deck* deck, unsigned int size) {
    for (unsigned int i = 0; i < size - 1; i++) {
        insertCard(deck->getCard());
    }

    Card card = deck->getCard();
    card.turn();
    insertCard(card);
}

bool Tableau::canInsert(Card card) {
    Card topCard = getTopCard();
    return (isEmpty() && card.getValue() == 13) ||
            ( !isEmpty() && (card.getValue() == topCard.getValue() - 1) && !card.hasSameSuitColor(topCard) );
}

void Tableau::accept(PileVisitor* pileVisitor) {
    pileVisitor->visit(this);
}
