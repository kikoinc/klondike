#include "models/Deck.hpp"

Deck::Deck() {
    std::vector<Suit> suits = Suit::getAll();
    for (std::vector<Suit>::iterator it = suits.begin(); it != suits.end(); ++it) {
        add_cards_from_suit(*it);
    }
}

void Deck::add_cards_from_suit(Suit suit) {
    for ( int i = 0; i < 13; i++) {
        Card card = Card(i + 1, suit, false);
        cards.insert(card);
    }
}

Deck::~Deck() {
    //delete deck;
}

Card Deck::getCard() {
    Card card = *(cards.begin());
    cards.erase(card);
    return card;
}

bool Deck::isEmpty() {
    return cards.empty();
}

int Deck::getSize() {
    return cards.size();
}
