#include "models/Waste.hpp"
#include "PileVisitor.hpp"

bool Waste::canInsert(Card card) {
    return true;
}

void Waste::accept(PileVisitor* pileVisitor) {
    pileVisitor->visit(this);
}
