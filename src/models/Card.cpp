#include "models/Card.hpp"

Card::Card(int value, Suit suit, bool turned):
    value(value), suit(suit), turned(turned) {
}

int Card::getValue() const {
    return value;
}

Suit Card::getSuit() const {
    return suit;
}

bool Card::isTurned() const {
    return turned;
}

void Card::turn() {
    turned = !turned;
}

bool Card::hasSameSuit(Card card) {
    return suit == card.getSuit();
}

bool Card::hasSameSuitColor(Card card) {
    return suit == card.suit;
}

std::string Card::getSuitStr() const {
    return getSuit().getSuitGraph();
}
