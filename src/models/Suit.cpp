#include "models/Suit.hpp"

const Suit Suit::CLUBS = Suit(SuitType::CLUBS);
const Suit Suit::DIAMONDS = Suit(SuitType::DIAMONDS);
const Suit Suit::HEARTS = Suit(SuitType::HEARTS);
const Suit Suit::SPADES = Suit(SuitType::SPADES);

const std::string GRAPH_CLUBS = "C";
const std::string GRAPH_DIAMONDS = "D";
const std::string GRAPH_HEARTS = "H";
const std::string GRAPH_SPADES = "S";

Suit::Suit(SuitType type):
    type(type) {
}

bool Suit::operator== (const Suit& suit) const {
    return type == suit.type;
}

bool Suit::hasSameColor(Suit suit) {
    if (type == suit.type) {
        return true;
    } else {
        if (type == SuitType::CLUBS) {
            return suit.type == SuitType::SPADES;
        }
        if (type == SuitType::SPADES) {
            return suit.type == SuitType::CLUBS;
        }
        if (type == SuitType::HEARTS) {
            return suit.type == SuitType::DIAMONDS;
        }
        if (type == SuitType::DIAMONDS) {
            return suit.type == SuitType::HEARTS;
        }
    }
    return false;
}

const std::string Suit::getSuitGraph() {
    switch(type) {
        case SuitType::CLUBS:
            return GRAPH_CLUBS;
        case SuitType::DIAMONDS:
            return GRAPH_DIAMONDS;
        case SuitType::HEARTS:
            return GRAPH_HEARTS;
        case SuitType::SPADES:
            return GRAPH_SPADES;
        default:
            return "-";
    }
}

std::vector<Suit> Suit::getAll() {
    std::vector<Suit> vector;
    vector.push_back(Suit::CLUBS);
    vector.push_back(Suit::DIAMONDS);
    vector.push_back(Suit::HEARTS);
    vector.push_back(Suit::SPADES);
    return vector;
}