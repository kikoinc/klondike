#include "models/Foundation.hpp"
#include "PileVisitor.hpp"

bool Foundation::canInsert(Card card) {
    return (isEmpty() && card.getValue() == 1) ||
            (!isEmpty() && (getTopCard().getValue() == card.getValue() - 1) && card.hasSameSuit(card));
}

void Foundation::accept(PileVisitor* pileVisitor) {
    pileVisitor->visit(this);
}