#include "models/Pile.hpp"

bool Pile::isEmpty() {
    return cards.empty();
}

int Pile::size() {
    return cards.size();
}

Card Pile::getTopCard() {
    return cards.top();
}

void Pile::removeTopCard() {
    cards.pop();
}

void Pile::insertCard(Card card) {
    cards.push(card);
}

void Pile::turnTopCard() {
    if (!isEmpty()) {
        cards.top().turn();
    }
}

std::vector<Card> Pile::getCards() {
    std::vector<Card> result;
    std::stack<Card> copy = cards;

    while(!copy.empty()) {
        result.push_back(copy.top());
        copy.pop();
    }

    return result;
}
