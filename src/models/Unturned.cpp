#include "models/Unturned.hpp"
#include "PileVisitor.hpp"

Unturned::Unturned(Deck* deck) {
    while (!deck->isEmpty()) {
        insertCard(deck->getCard());
    }
}

bool Unturned::canInsert(Card card) {
    return !isEmpty();
}

void Unturned::accept(PileVisitor* pileVisitor) {
    pileVisitor->visit(this);
}