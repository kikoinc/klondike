#include "models/State.hpp"

const State State::START = State(StateType::START);
const State State::IN_GAME = State(StateType::IN_GAME);
const State State::WIN = State(StateType::WIN);
const State State::END = State(StateType::END);

State::State(StateType type) :
    type(type) {
}

bool State::operator== (const State& state) const {
    return type == state.type;
}
