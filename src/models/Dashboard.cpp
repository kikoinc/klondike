#include "models/Dashboard.hpp"

Dashboard::Dashboard() {
    initializeFoundations();
    initializeTableaus();
    waste = new Waste();
    unturned = new Unturned(&deck);
}

Dashboard::~Dashboard() {
    delete unturned;
    delete waste;
    for (unsigned int i = 0; i < foundations.size(); i++) {
        delete foundations[i];
    }
    for (unsigned int i = 0; i < tableaus.size(); i++) {
        delete tableaus[i];
    }
}

void Dashboard::initializeFoundations() {
    for (unsigned int i = 0; i < foundations.size(); i++) {
        foundations[i] = new Foundation();
    }
}

void Dashboard::initializeTableaus() {
    for (unsigned int i = 0; i < tableaus.size(); i++) {
        tableaus[i] = new Tableau(&deck, i + 1);
    }
}

bool Dashboard::is_winner() {
    for (Pile* foundation: foundations) {
        if (foundation->size() != 12) {
            return false;
        }
    }

    return true;
}

Pile* Dashboard::getUnturned() {
    return unturned;
}

Pile* Dashboard::getWaste() {
    return waste;
}

Pile* Dashboard::getFoundation(int id) {
    return foundations[id];
}

Pile* Dashboard::getTableau(int id) {
    return tableaus[id];
}