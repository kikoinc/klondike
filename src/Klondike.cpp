#include "Klondike.hpp"

Klondike::Klondike(View* view) {
    assert(view != nullptr);
    this->view = view;
}

void Klondike::play() {
    Controller* controller;
    do {
        controller = logic.getController();
        if (controller != nullptr) {
            view->interact(controller);
        }
    } while(controller != nullptr);
}
