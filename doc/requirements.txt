El tablero se compone de los siguientes elementos:
    - Un mazo, donde el jugador podrá sacar cartas.
    - El mazo de cartas sacadas.
    - Cuatro pilas donde el jugador podrá ir poniendo las cartas ordenadas según el palo.
    - Siete pilas:
        - 1: con una carta descubierta.
        - 2: con una carta cubierta y una descubierta.
        - 3: con dos cartas cubiertas y una descubierta.
        - 4: con tres cartas cubiertas y una descubierta.
        - 5: con cuatro cartas cubiertas y una descubierta.
        - 6: con cinco cartas cubiertas y una descubierta.
        - 7: con seis cartas cubiertas y una descubierta.


   [M]  [MD]     [PR1] [PR2] [PR3] [PR4]

     [P1] [P2] [P3] [P4] [P5] [P6] [P7]

   M: mazo
   MD: mazo descubierto
   PRn: pila resultado n
   Pn: pila n

Las acciones que puede hacer un jugador son:
    - Sacar una carta del mazo.
    - Mover una carta del mazo descubierto a una pila.
    - Mover una carta del mazo descubierto a una pila de resultado.
    - Mover una carta de una pila a otra.
    - Mover una carta de una pila a otra de resultado.