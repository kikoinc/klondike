@startuml
title PileView

interface PileVisitor {
    + visit(Unturned*)
    + visit(Waste*)
    + visit(Foundation*)
    + visit(Tableau*)
}

interface PileVisitable {
    + accept(PileVisitor*)
}

class PileView {
    - Pile* pile;
    - std::string title;

    + PileView(Pile*);
    + void write(std::string);
    + void visit(Unturned*);
    + void visit(Waste*);
    + void visit(Foundation*);
    + void visit(Tableau*);
}

abstract class Pile {
    # std::stack<Card> cards;

    {abstract} virtual bool canInsert(Card);
    {abstract} virtual ~Pile() = default;

    + bool isEmpty();
    + int size();
    + Card getTopCard();
    + void removeTopCard();
    + void insertCard(Card);
    + void TurnTopCard();
    + std::vector<Card> getCards();
}

class Unturned {
    + Unturned(Deck*);
    + bool canInsert(Card);
    + void accept(PileVisitor*);
}

class Waste {
    + bool canInsert(Card);
    + void accept(PileVisitor*);
}

class Foundation {
    + bool canInsert(Card);
    + void accept(PileVisitor*);
}

class Tableau {
    + Tableau(Deck*, unsigned int size);
    + bool canInsert(Card);
    + void accept(PileVisitor*);
}

PileVisitable <|-- Pile
Pile <|-- Unturned
Pile <|-- Waste
Pile <|-- Foundation
Pile <|-- Tableau

PileVisitor <|-- PileView

PileVisitor ..> Unturned
PileVisitor ..> Waste
PileVisitor ..> Foundation
PileVisitor ..> Tableau

PileVisitable ..> PileVisitor


@enduml