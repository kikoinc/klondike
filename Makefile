EXE = klondike

SRC_DIR = src
OBJ_DIR = obj
BIN_DIR = bin

SRCS    := $(shell find $(SRC_DIR) -name '*.cpp')
SRCDIRS := $(shell find . -name '*.cpp' -exec dirname {} \; | uniq)
OBJS    := $(patsubst %.cpp,$(OBJ_DIR)/%.o,$(SRCS))

DEBUG = -g
INCLUDES = -I./include
CFLAGS += -Wall -pedantic -ansi -c $(DEBUG) $(INCLUDES) -std=c++11
LDFLAGS +=
LDLIBS +=

.PHONY: all clean distclean

all: $(BIN_DIR)/$(EXE)

$(BIN_DIR)/$(EXE): buildrepo $(OBJS)
	@mkdir -p `dirname $@`
	@echo "Linking $@..."
	@$(CXX) $(OBJS) $(LDFLAGS) -o $@

$(OBJ_DIR)/%.o: %.cpp
	@echo "Generating dependencies for $<..."
	@$(call make-depend,$<,$@,$(subst .o,.d,$@))
	@echo "Compiling $<..."
	@$(CXX) $(CFLAGS) $< -o $@

clean:
	$(RM) -r $(OBJ_DIR)

distclean: clean
	$(RM) -r $(BIN_DIR)

buildrepo:
	@$(call make-repo)

define make-repo
   for dir in $(SRCDIRS); \
   do \
	mkdir -p $(OBJ_DIR)/$$dir; \
   done
endef

# usage: $(call make-depend,source-file,object-file,depend-file)
define make-depend
  $(CXX) -MM       \
         -MF $3    \
         -MP       \
         -MT $2    \
         $(CFLAGS) \
         $1
endef
