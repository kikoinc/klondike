#ifndef KLONDIKE_PILEVISITOR_HPP
#define KLONDIKE_PILEVISITOR_HPP

class Unturned;
class Waste;
class Foundation;
class Tableau;

class PileVisitor {
public:
    virtual ~PileVisitor() = default;
    virtual void visit(Unturned*) = 0;
    virtual void visit(Waste*) = 0;
    virtual void visit(Foundation*) = 0;
    virtual void visit(Tableau*) = 0;
};


#endif //KLONDIKE_PILEVISITOR_HPP
