#ifndef KLONDIKE_GAME_H
#define KLONDIKE_GAME_H

#include <assert.h>

#include "models/State.hpp"
#include "models/Dashboard.hpp"

class Game {
public:
    Game();
    ~Game();
    const State getState();
    void setState(const State* state);
    Dashboard* getDashboard();
    bool isWinner();
private:
    const State* state;
    Dashboard* dashboard;
};

#endif //KLONDIKE_GAME_H
