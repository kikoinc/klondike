#ifndef KLONDIKE_PILE_H
#define KLONDIKE_PILE_H

#include <vector>
#include <stack>
#include <iostream>

#include "models/Card.hpp"
#include "utils/Logger.hpp"
#include "PileVisitable.hpp"

class Pile: public PileVisitable {
public:
    virtual bool canInsert(Card card) = 0;
    virtual ~Pile() = default;

    bool isEmpty();
    int size();
    Card getTopCard();
    void removeTopCard();
    void insertCard(Card Card);
    void turnTopCard();
    std::vector<Card> getCards();
protected:
    Logger logger;
    std::stack<Card> cards;
};


#endif //KLONDIKE_PILE_H
