#ifndef KLONDIKE_FOUNDATION_H
#define KLONDIKE_FOUNDATION_H

#include "Pile.hpp"

class PileVisitor;

class Foundation: public Pile {
public:
    bool canInsert(Card card);
    void accept(PileVisitor* pileVisitor);
};

#endif //KLONDIKE_FOUNDATION_H
