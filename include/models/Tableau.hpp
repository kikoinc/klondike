#ifndef KLONDIKE_TABLEAU_H
#define KLONDIKE_TABLEAU_H

#include "models/Pile.hpp"
#include "models/Deck.hpp"
#include "models/Card.hpp"

class PileVisitor;

class Tableau: public Pile {
public:
    Tableau(Deck* deck, unsigned int size);
    bool canInsert(Card card);
    void accept(PileVisitor* pileVisitor);
};


#endif //KLONDIKE_TABLEAU_H
