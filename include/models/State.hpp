#ifndef KLONDIKE_STATE_H
#define KLONDIKE_STATE_H

class State {
public:
    static const State START;
    static const State IN_GAME;
    static const State WIN;
    static const State END;

    bool operator== (const State& state) const;
private:
    enum class StateType {
        START,
        IN_GAME,
        WIN,
        END
    };

    State(StateType type);
    StateType type;
};

#endif //KLONDIKE_STATE_H
