#ifndef KLONDIKE_DECK_H
#define KLONDIKE_DECK_H

#include <iostream>
#include <unordered_set>
#include "models/Card.hpp"
#include "models/Suit.hpp"

struct CardHasher
{
    size_t operator()(const Card& obj) const
    {
        return std::hash<std::string>()(std::to_string(obj.getValue()) + obj.getSuitStr());
    }
};

struct CardComparator
{
    bool operator()(const Card& obj1, const Card& obj2) const
    {
        return (obj1.getValue() == obj2.getValue() && obj1.getSuit() == obj2.getSuit());
    }
};

class Deck {
public:
    Deck();
    ~Deck();
    Card getCard();
    bool isEmpty();
    int getSize();
private:
    std::unordered_set<Card, CardHasher, CardComparator> cards;
    void add_cards_from_suit(Suit suit);
};

#endif //KLONDIKE_DECK_H
