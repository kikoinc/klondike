#ifndef KLONDIKE_DASHBOARD_H
#define KLONDIKE_DASHBOARD_H

#include <array>
#include <string>
#include <assert.h>

#include "models/Deck.hpp"
#include "models/Unturned.hpp"
#include "models/Waste.hpp"
#include "models/Foundation.hpp"
#include "models/Tableau.hpp"
#include "utils/Logger.hpp"

class Dashboard {
public:
    static const int NUMBER_FOUNDATIONS = 4;
    static const int NUMBER_TABLEAUS = 7;

    Dashboard();
    ~Dashboard();
    bool is_winner();
    Pile* getUnturned();
    Pile* getWaste();
    Pile* getFoundation(int id);
    Pile* getTableau(int id);
private:
    const int MIN_CARDS = 29;

    Logger logger;

    Deck deck;
    Pile* unturned;
    Pile* waste;

    std::array<Pile*, NUMBER_FOUNDATIONS> foundations;
    std::array<Pile*, NUMBER_TABLEAUS> tableaus;

    void initializeFoundations();
    void initializeTableaus();
    void drawTableaus();
    void drawFoundations();
};


#endif //KLONDIKE_DASHBOARD_H
