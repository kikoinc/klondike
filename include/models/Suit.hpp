#ifndef KLONDIKE_SUIT_H
#define KLONDIKE_SUIT_H

#include <string>
#include <vector>

class Suit {
public:
    static const Suit CLUBS;
    static const Suit DIAMONDS;
    static const Suit HEARTS;
    static const Suit SPADES;
    static std::vector<Suit> getAll();

    bool operator== (const Suit& suit) const;
    bool hasSameColor(Suit suit);
    const std::string getSuitGraph();
private:
    enum class SuitType {
        CLUBS,
        DIAMONDS,
        HEARTS,
        SPADES
    };

    SuitType type;

    Suit(SuitType type);
};

#endif //KLONDIKE_SUIT_H
