#ifndef KLONDIKE_CARD_H
#define KLONDIKE_CARD_H

#include <sstream>
#include <string>
#include <assert.h>

#include "Suit.hpp"

class Card {
public:
    Card(int value, Suit suit, bool turned);
    int getValue() const;
    Suit getSuit() const;
    bool isTurned() const;
    void turn();
    bool hasSameSuit(Card card);
    bool hasSameSuitColor(Card card);
    std::string getSuitStr() const;
private:
    int value;
    Suit suit;
    bool turned;
};


#endif //KLONDIKE_CARD_H
