#ifndef KLONDIKE_UNTURNED_H
#define KLONDIKE_UNTURNED_H

#include "models/Pile.hpp"
#include "models/Deck.hpp"

class PileVisitor;

class Unturned: public Pile {
public:
    Unturned(Deck* deck);
    bool canInsert(Card card);
    void accept(PileVisitor* pileVisitor);
};


#endif //KLONDIKE_UNTURNED_H
