#ifndef KLONDIKE_WASTE_H
#define KLONDIKE_WASTE_H

#include "Pile.hpp"

class PileVisitor;

class Waste: public Pile {
public:
    bool canInsert(Card card);
    void accept(PileVisitor* pileVisitor);
};


#endif //KLONDIKE_WASTE_H
