#ifndef KLONDIKE_LOGGER_H
#define KLONDIKE_LOGGER_H

#include <iostream>
#include <string>

class Logger {
public:
    void debug(char const* message);
    void debug(std::string message);
    void debug(int n);
};


#endif //KLONDIKE_LOGGER_H
