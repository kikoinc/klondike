#ifndef KLONDIKE_IO_H
#define KLONDIKE_IO_H

#include <iostream>
#include <string>
#include <exception>

class IO {
public:
    std::string readString(std::string title);
    int readInt(std::string title);
    void write(std::string string);
    void write(int n);
    void writeLine(std::string string);
};


#endif //KLONDIKE_IO_H
