#ifndef KLONDIKE_LIMITEDINTDIALOG_H
#define KLONDIKE_LIMITEDINTDIALOG_H

#include <sstream>
#include "utils/ClosedInterval.hpp"
#include "utils/IO.hpp"

class LimitedIntDialog {
public:
    LimitedIntDialog(std::string title, int min, int max);
    ~LimitedIntDialog();
    int read();
private:
    std::string title;
    ClosedInterval* interval;
    IO io;
};


#endif //KLONDIKE_LIMITEDINTDIALOG_H
