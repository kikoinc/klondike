#ifndef KLONDIKE_CLOSEDINTERVAL_H
#define KLONDIKE_CLOSEDINTERVAL_H

class ClosedInterval {
public:
    ClosedInterval(int min, int max);
    int getMin();
    int getMax();
    bool includes(int value);
private:
    int min;
    int max;
};

#endif //KLONDIKE_CLOSEDINTERVAL_H
