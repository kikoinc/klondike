#ifndef KLONDIKE_PILEVISITABLE_HPP
#define KLONDIKE_PILEVISITABLE_HPP

class PileVisitor;

class PileVisitable {
public:
    virtual ~PileVisitable() = default;
    virtual void accept(PileVisitor* pileVisitor) = 0;
};

#endif //KLONDIKE_PILEVISITABLE_HPP
