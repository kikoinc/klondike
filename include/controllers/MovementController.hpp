#ifndef KLONDIKE_MOVEMENTCONTROLLER_H
#define KLONDIKE_MOVEMENTCONTROLLER_H

#include <assert.h>

#include "controllers/Controller.hpp"
#include "controllers/MovementControllerInterface.hpp"
#include "controllers/ControllerVisitor.hpp"
#include "controllers/Error.hpp"
#include "models/Game.hpp"
#include "models/Pile.hpp"
#include "models/State.hpp"
#include "PileVisitor.hpp"
#include "utils/Logger.hpp"

class MovementController: public Controller, public MovementControllerInterface, public PileVisitor {
public:
    MovementController(Game* game);
    void accept(ControllerVisitor* controllerVisitor);
    void move();
    Dashboard* getDashboard();
    void setOrigin(Pile*);
    void setDestination(Pile*);
    const Error* validateMovement(Pile*, Pile*);

    void visit(Unturned*);
    void visit(Waste*);
    void visit(Foundation*);
    void visit(Tableau*);
private:
    Pile* origin;
    Pile* destination;
    Logger logger;
};


#endif //KLONDIKE_MOVEMENTCONTROLLER_H
