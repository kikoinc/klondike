#ifndef KLONDIKE_STARTCONTROLLERINTERFACE_HPP
#define KLONDIKE_STARTCONTROLLERINTERFACE_HPP

#include "PresenterControllerInterface.hpp"

class StartControllerInterface: public PresenterControllerInterface {
public:
    virtual ~StartControllerInterface() = default;
    virtual void start() = 0;
};


#endif //KLONDIKE_STARTCONTROLLERINTERFACE_HPP
