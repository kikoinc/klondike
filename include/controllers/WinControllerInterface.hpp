#ifndef KLONDIKE_WINCONTROLLERINTERFACE_HPP
#define KLONDIKE_WINCONTROLLERINTERFACE_HPP

#include "PresenterControllerInterface.hpp"

class WinControllerInterface: public PresenterControllerInterface {
public:
    virtual ~WinControllerInterface() = default;
    virtual void win() = 0;
};

#endif //KLONDIKE_WINCONTROLLERINTERFACE_HPP
