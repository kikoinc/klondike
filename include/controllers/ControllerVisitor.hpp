#ifndef KLONDIKE_CONTROLLERVISITOR_HPP
#define KLONDIKE_CONTROLLERVISITOR_HPP

#include "controllers/StartControllerInterface.hpp"
#include "controllers/MovementControllerInterface.hpp"
#include "controllers/WinControllerInterface.hpp"

class ControllerVisitor {
public:
    virtual ~ControllerVisitor() = default;
    virtual void visit(StartControllerInterface*) = 0;
    virtual void visit(MovementControllerInterface*) = 0;
    virtual void visit(WinControllerInterface*) = 0;
};

#endif //KLONDIKE_CONTROLLERVISITOR_HPP
