#ifndef KLONDIKE_LOGIC_H
#define KLONDIKE_LOGIC_H

#include "controllers/Controller.hpp"
#include "controllers/StartController.hpp"
#include "controllers/MovementController.hpp"
#include "controllers/WinController.hpp"
#include "models/Game.hpp"

class Logic {
public:
    Logic();
    ~Logic();
    Controller* getController();
private:
    Game game;
    StartController* startController;
    MovementController* movementController;
    WinController* winController;
};


#endif //KLONDIKE_LOGIC_H
