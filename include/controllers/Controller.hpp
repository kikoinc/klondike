#ifndef KLONDIKE_CONTROLLER_H
#define KLONDIKE_CONTROLLER_H

#include "ControllerInterface.hpp"
#include "models/Game.hpp"
#include "models/Dashboard.hpp"
#include "ControllerVisitor.hpp"

class Controller: public ControllerInterface {
public:
    virtual ~Controller() = default;
    virtual void accept(ControllerVisitor* controllerVisitor) = 0;
    Dashboard* getDashboard();
protected:
    Game* getGame();
    void setGame(Game* game);
private:
    Game* game;
};

#endif //KLONDIKE_CONTROLLER_H
