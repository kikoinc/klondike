#ifndef KLONDIKE_PRESENTERCONTROLLER_HPP
#define KLONDIKE_PRESENTERCONTROLLER_HPP

#include "models/Dashboard.hpp"

class PresenterControllerInterface {
public:
    virtual ~PresenterControllerInterface() = default;
    virtual Dashboard* getDashboard() = 0;
};

#endif //KLONDIKE_PRESENTERCONTROLLER_HPP
