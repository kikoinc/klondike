#ifndef KLONDIKE_CONTROLLERINTERFACE_HPP
#define KLONDIKE_CONTROLLERINTERFACE_HPP

#include "ControllerVisitor.hpp"

class ControllerInterface {
public:
    virtual ~ControllerInterface() = default;
    virtual void accept(ControllerVisitor* controllerVisitor) = 0;
};

#endif //KLONDIKE_CONTROLLERINTERFACE_HPP
