#ifndef KLONDIKE_MOVEMENTCONTROLLERINTERFACE_HPP
#define KLONDIKE_MOVEMENTCONTROLLERINTERFACE_HPP

#include "controllers/PresenterControllerInterface.hpp"
#include "controllers/Error.hpp"
#include "models/Pile.hpp"

class MovementControllerInterface: public PresenterControllerInterface {
public:
    virtual ~MovementControllerInterface() = default;
    virtual void setOrigin(Pile*) = 0;
    virtual void setDestination(Pile*) = 0;
    virtual const Error* validateMovement(Pile*, Pile*) = 0;
    virtual void move() = 0;
};

#endif //KLONDIKE_MOVEMENTCONTROLLERINTERFACE_HPP
