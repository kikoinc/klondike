#ifndef KLONDIKE_WINCONTROLLER_HPP
#define KLONDIKE_WINCONTROLLER_HPP

#include "controllers/Controller.hpp"
#include "controllers/WinControllerInterface.hpp"
#include "models/Game.hpp"

class WinController: public Controller, public WinControllerInterface {
public:
    WinController(Game* game);
    void accept(ControllerVisitor* controllerVisitor);
    void win();
    Dashboard* getDashboard();
};


#endif //KLONDIKE_WINCONTROLLER_HPP
