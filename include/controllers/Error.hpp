#ifndef KLONDIKE_ERROR_HPP
#define KLONDIKE_ERROR_HPP

#include <string>

class Error {
public:
    static const Error MOVEMENT_NOT_ALLOWED;
    static const Error PILE_IS_EMPTY;

    std::string getMessage() const;
private:
    Error(std::string message);
    std::string message;
};


#endif //KLONDIKE_ERROR_HPP
