#ifndef KLONDIKE_STARTCONTROLLER_HPP
#define KLONDIKE_STARTCONTROLLER_HPP

#include <assert.h>

#include "controllers/Controller.hpp"
#include "controllers/StartControllerInterface.hpp"
#include "controllers/ControllerVisitor.hpp"
#include "models/Game.hpp"
#include "models/State.hpp"

class StartController: public Controller, public StartControllerInterface {
public:
    StartController(Game* game);
    void accept(ControllerVisitor* controllerVisitor);
    void start();
    Dashboard* getDashboard();
};

#endif //KLONDIKE_STARTCONTROLLER_HPP
