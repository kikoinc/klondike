#ifndef KLONDIKE_VIEW_HPP
#define KLONDIKE_VIEW_HPP

#include "controllers/ControllerVisitor.hpp"
#include "controllers/ControllerInterface.hpp"
#include "controllers/StartControllerInterface.hpp"
#include "controllers/MovementControllerInterface.hpp"

class View: public ControllerVisitor {
public:
    virtual ~View() = default;
    virtual void interact(ControllerInterface* controllerInterface) = 0;
    virtual void visit(StartControllerInterface* startController) = 0;
    virtual void visit(MovementControllerInterface* movementController) = 0;
};

#endif //KLONDIKE_VIEW_HPP
