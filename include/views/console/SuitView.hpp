#ifndef KLONDIKE_SUITVIEW_HPP
#define KLONDIKE_SUITVIEW_HPP

#include <assert.h>

#include "models/Suit.hpp"
#include "utils/IO.hpp"

namespace ConsoleView {
    class SuitView {
    public:
        SuitView(Suit suit);
        void write();

    private:
        Suit suit;
        IO io;
    };
}

#endif //KLONDIKE_SUITVIEW_HPP
