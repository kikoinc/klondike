#ifndef KLONDIKE_WINVIEW_HPP
#define KLONDIKE_WINVIEW_HPP

#include "controllers/WinControllerInterface.hpp"
#include "utils/IO.hpp"

namespace ConsoleView {
    class WinView {
    public:
        void interact(WinControllerInterface*);
    private:
        IO io;
    };
}


#endif //KLONDIKE_WINVIEW_HPP
