#ifndef KLONDIKE_DASHBOARDVIEW_HPP
#define KLONDIKE_DASHBOARDVIEW_HPP

#include <assert.h>

#include "controllers/PresenterControllerInterface.hpp"

namespace ConsoleView {
    class DashboardView {
    public:
        DashboardView(PresenterControllerInterface *presenterController);
        void write();

    private:
        PresenterControllerInterface *presenterController;
    };
}

#endif //KLONDIKE_DASHBOARDVIEW_HPP
