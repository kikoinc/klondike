#ifndef KLONDIKE_ASKMOVEMENTVIEW_HPP
#define KLONDIKE_ASKMOVEMENTVIEW_HPP

#include "controllers/MovementControllerInterface.hpp"
#include "controllers/Error.hpp"
#include "utils/LimitedIntDialog.hpp"
#include "utils/IO.hpp"
#include "utils/Logger.hpp"

namespace ConsoleView {
    class AskMovementView {
    public:
        const Error* interact(MovementControllerInterface*);
    private:
        Logger logger;
        IO io;
        MovementControllerInterface* moveController;

        Pile* askForPile(std::string title);
        void printAvailableMovements();
        void printAvailablePiles(std::string title);
        void printPilesPerType(std::string pileName, int number);
    };
}


#endif //KLONDIKE_ASKMOVEMENTVIEW_HPP
