#ifndef KLONDIKE_GAMEVIEW_HPP
#define KLONDIKE_GAMEVIEW_HPP

#include "controllers/MovementControllerInterface.hpp"
#include "views/console/DashboardView.hpp"
#include "models/Dashboard.hpp"
#include "views/console/AskMovementView.hpp"

namespace ConsoleView {
    class GameView {
    public:
        void interact(MovementControllerInterface *);

        void printBadMovement();
        void printEndGame();
    private:
        IO io;
        AskMovementView askMovementView;
    };
}


#endif //KLONDIKE_GAMEVIEW_HPP
