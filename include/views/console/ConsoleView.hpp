#ifndef KLONDIKE_CONSOLEVIEW_H
#define KLONDIKE_CONSOLEVIEW_H

#include <assert.h>

#include "controllers/ControllerInterface.hpp"
#include "controllers/StartControllerInterface.hpp"
#include "controllers/MovementControllerInterface.hpp"
#include "controllers/WinControllerInterface.hpp"
#include "views/console/StartView.hpp"
#include "views/console/GameView.hpp"
#include "views/console/WinView.hpp"
#include "views/View.hpp"

namespace ConsoleView {
    class ConsoleView : public View {
    public:
        void interact(ControllerInterface*);
        void visit(StartControllerInterface*);
        void visit(MovementControllerInterface*);
        void visit(WinControllerInterface*);

    private:
        StartView startView;
        GameView gameView;
        WinView winView;
    };
}

#endif //KLONDIKE_CONSOLEVIEW_H
