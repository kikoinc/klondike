#ifndef KLONDIKE_PILEVIEW_HPP
#define KLONDIKE_PILEVIEW_HPP

#include <assert.h>
#include <string>

#include "models/Pile.hpp"
#include "PileVisitor.hpp"
#include "models/Unturned.hpp"
#include "models/Waste.hpp"
#include "models/Foundation.hpp"
#include "models/Tableau.hpp"
#include "utils/IO.hpp"

namespace ConsoleView {
    class PileView : public PileVisitor {
    public:
        PileView(Pile *pile);
        void write(std::string title);
        void visit(Unturned *unturned);
        void visit(Waste *waste);
        void visit(Foundation *foundation);
        void visit(Tableau *tableau);

    private:
        Pile *pile;
        IO io;

        void printCard(Card);
    };
}

#endif //KLONDIKE_PILEVIEW_HPP
