#ifndef KLONDIKE_STARTVIEW_HPP
#define KLONDIKE_STARTVIEW_HPP

#include "controllers/StartControllerInterface.hpp"
#include "views/console/DashboardView.hpp"

namespace ConsoleView {
    class StartView {
    public:
        void interact(StartControllerInterface*);
    };
}

#endif //KLONDIKE_STARTVIEW_HPP
