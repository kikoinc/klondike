#ifndef KLONDIKE_CARDVIEW_HPP
#define KLONDIKE_CARDVIEW_HPP

#include "models/Card.hpp"

namespace ConsoleView {
    class CardView {
    public:
        CardView(Card card);
        void write();
    private:
        Card card;
    };
}


#endif //KLONDIKE_CARDVIEW_HPP
