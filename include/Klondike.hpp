#ifndef KLONDIKE_KLONDIKE_H
#define KLONDIKE_KLONDIKE_H

#include "controllers/Logic.hpp"
#include "controllers/Controller.hpp"
#include "views/View.hpp"

class Klondike {
public:
    Klondike(View* view);
    void play();
private:
    Logic logic;
    View* view;
};

#endif //KLONDIKE_KLONDIKE_H
